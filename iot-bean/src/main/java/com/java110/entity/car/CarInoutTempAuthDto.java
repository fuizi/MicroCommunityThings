package com.java110.entity.car;

import com.java110.entity.PageDto;
import com.java110.entity.parkingCouponCar.ParkingCouponCarDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 临时车进场审核
 */
public class CarInoutTempAuthDto extends PageDto implements Serializable {

    public static final String STATE_W = "W";// 待审核
    public static final String STATE_C = "C";// 审核完成
    public static final String STATE_F = "F";// 审核决绝

    private String authId;
    private String carNum;
    private String carType;
    private String communityId;
    private String paId;

    private String areaNum;

    private String[] paIds;
    private String machineId;
    private String machineCode;
    private String machineName;
    private String phoneJpg;
    private String state;
    private String[] states;
    private String msg;
    private String remark;
    private String statusCd;
    private String createTime;

    private String newCarNum;

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getPaId() {
        return paId;
    }

    public void setPaId(String paId) {
        this.paId = paId;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getPhoneJpg() {
        return phoneJpg;
    }

    public void setPhoneJpg(String phoneJpg) {
        this.phoneJpg = phoneJpg;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String[] getPaIds() {
        return paIds;
    }

    public void setPaIds(String[] paIds) {
        this.paIds = paIds;
    }

    public String[] getStates() {
        return states;
    }

    public void setStates(String[] states) {
        this.states = states;
    }

    public String getAreaNum() {
        return areaNum;
    }

    public void setAreaNum(String areaNum) {
        this.areaNum = areaNum;
    }

    public String getNewCarNum() {
        return newCarNum;
    }

    public void setNewCarNum(String newCarNum) {
        this.newCarNum = newCarNum;
    }
}
