package com.java110.core.service.car.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.constant.SystemConstant;
import com.java110.core.dao.ICarInoutTempAuthServiceDao;
import com.java110.core.entity.car.BarrierGateControlDto;
import com.java110.core.service.app.IAppService;
import com.java110.core.service.car.ICarInoutTempAuthService;
import com.java110.core.service.community.ICommunityService;
import com.java110.core.service.fee.ITempCarFeeConfigService;
import com.java110.core.service.hc.ICarCallHcService;
import com.java110.entity.PageDto;
import com.java110.entity.car.CarInoutTempAuthDto;
import com.java110.entity.machine.MachineDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @ClassName CarInoutServiceImpl
 * @Description TODO 车辆进出场服务类
 * @Author wuxw
 * @Date 2020/5/14 14:49
 * @Version 1.0
 * add by wuxw 2020/5/14
 **/

@Service("carInoutTempAuthServiceImpl")
public class CarInoutTempAuthServiceImpl implements ICarInoutTempAuthService {

    @Autowired
    private ICarInoutTempAuthServiceDao carInoutTempAuthServiceDao;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ICommunityService communityServiceImpl;

    @Autowired
    private IAppService appServiceImpl;

    @Autowired
    private ICarCallHcService carCallHcServiceImpl;

    @Autowired
    private ITempCarFeeConfigService tempCarFeeConfigServiceImpl;

    /**
     * 添加小区信息
     *
     * @param carInoutTempAuthDto 小区对象
     * @return
     */
    @Override
    public int saveCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto, MachineDto machineDto) {

        int count = carInoutTempAuthServiceDao.saveCarInoutTempAuth(carInoutTempAuthDto);
        if (count < 1) {
            return count;
        }

        BarrierGateControlDto barrierGateControlDto
                = new BarrierGateControlDto();
        barrierGateControlDto.setAction(BarrierGateControlDto.ACTION_TEMP_CAR_AUTH);
        barrierGateControlDto.setMachineId(carInoutTempAuthDto.getMachineId());
        barrierGateControlDto.setBody(carInoutTempAuthDto);

        try {
            carCallHcServiceImpl.carInoutPageInfo(barrierGateControlDto, machineDto.getLocationObjId(), machineDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * 获取车辆信息
     *
     * @param carInoutTempAuthDto 车辆信息
     * @return
     * @throws Exception
     */
    public long queryCarInoutTempAuthCount(CarInoutTempAuthDto carInoutTempAuthDto) {

        long count = carInoutTempAuthServiceDao.queryCarInoutTempAuthCount(carInoutTempAuthDto);
        //刷新人脸地
        return count;
    }

    /**
     * 获取车辆信息
     *
     * @param carInoutTempAuthDto 车辆信息
     * @return
     * @throws Exception
     */
    public List<CarInoutTempAuthDto> queryCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) {
        if (carInoutTempAuthDto.getPage() != PageDto.DEFAULT_PAGE) {
            carInoutTempAuthDto.setPage((carInoutTempAuthDto.getPage() - 1) * carInoutTempAuthDto.getRow());
        }
        List<CarInoutTempAuthDto> carInoutTempAuthDtos = null;
        carInoutTempAuthDtos = carInoutTempAuthServiceDao.getCarInoutTempAuths(carInoutTempAuthDto);
        //刷新人脸地
        return carInoutTempAuthDtos;
    }

    @Override
    public int updateCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) {
        int count = carInoutTempAuthServiceDao.updateCarInoutTempAuth(carInoutTempAuthDto);
        return count;
    }

    @Override
    public int deleteCarInoutTempAuth(CarInoutTempAuthDto carInoutTempAuthDto) {
        carInoutTempAuthDto.setStatusCd(SystemConstant.STATUS_INVALID);
        int count = carInoutTempAuthServiceDao.updateCarInoutTempAuth(carInoutTempAuthDto);

        return count;
    }


}
